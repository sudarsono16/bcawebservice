package com.bca.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.servlet.http.HttpServletResponse;

import adapter.HeaderAdapter;
import adapter.TokenAdapter;
import adapter.EncryptAdapter;
import adapter.InsecureTrustManagerAdapter;
import model.Globals;

@RestController
public class controller {
    	private static final Logger logger = Logger.getLogger(controller.class);
    	

	@RequestMapping(value = "/ping",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody void GetPing()
	{
		return;
	}

	@RequestMapping(value = "/callAPI",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody String CallAPI(@RequestBody model.mdlAPI mdlAPI, HttpServletResponse response) throws NamingException, KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, GeneralSecurityException, IOException
	{
	   	long startTime = System.currentTimeMillis();
		long stopTime = 0;
		long elapsedTime = 0;
		
		String jsonResult = "";
		Globals.keyAPI = mdlAPI.keyAPI;
		Gson gson = new Gson();
		// Get the base naming context from web.xml
		Context context = (Context)new InitialContext().lookup("java:comp/env");

		// Get a single value from web.xml
		String APIGatewayIPAddress = (String)context.lookup("param_ip_api_gw");
		String XBCAClientID = (String)context.lookup("param_xbca_clientid");
		String APISecret = (String)context.lookup("param_apisecret");
		String APIKey = (String)context.lookup("param_apikey");

		String decryptedAPISecret = EncryptAdapter.decrypt(APISecret, Globals.keyAPI);
		String decryptedAPIKey = EncryptAdapter.decrypt(APIKey, Globals.keyAPI);
		String decryptedXBCAClientID = EncryptAdapter.decrypt(XBCAClientID, Globals.keyAPI);
		
		if (mdlAPI.urlAPI.contains("/passbooks/small-passbooks/account-statements")){
////		    jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"Transaction\":\"050417 KAS            4,000.00 K     610,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            5,000.00 K     615,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            6,000.00 K     621,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            7,000.00 K     628,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            8,000.00 K     636,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           10,000.00 K    646,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           11,000.00 K    657,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           12,000.00 K    669,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           13,000.00 K    682,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"}]}";
////		    jsonResult = "{\"TransactionIndicator\":\"MC\",\"TransactionRecord\":[{\"Transaction\":\"100616 KAS            1,000.00 K       67,116,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"}]}";
		    if (mdlAPI.contentAPI.contains("0100000181")){
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"Transaction\":\"100616 KAS            1,000.00 K       67,116,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,117,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,118,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,119,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,120,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,121,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,122,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,123,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,124,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,125,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,126,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"\",\"Trailer\":\"*********************************************************\"}]}";
			jsonResult = "{\"ErrorCode\":\"ESB-18-293\", \"ErrorMessage\":{\"Indonesian\":\"Tidak ada transaksi yang dapat dicetak\" , \"English\":\"No transaction to print\"}}";
		    }else if (mdlAPI.contentAPI.contains("2580001508")){
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"Transaction\":\"100616 KAS            1,000.00 K       67,116,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,117,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,118,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,119,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,120,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,121,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,122,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,123,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,124,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,125,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,126,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"\",\"Trailer\":\"*********************************************************\"}]}";
			jsonResult = "{\"ErrorCode\":\"ESB-18-296\", \"ErrorMessage\":{\"Indonesian\":\"Terdapat indikasi tidak memiliki buku\" , \"English\":\"No book\"}}";
		    }
//		    else{
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"Transaction\":\"100616 KAS            1,000.00 K       67,116,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,117,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,118,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,119,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,120,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,121,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,122,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,123,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,124,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,125,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"100616 KAS            1,000.00 K       67,126,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"},{\"Transaction\":\"\",\"Trailer\":\"*********************************************************\"}]}";
//		    }
		    return jsonResult;
		}
		
		if (mdlAPI.urlAPI.contains("/passbooks/regular-passbooks/account-statements")){
		    if (mdlAPI.contentAPI.contains("2580001508")){
			jsonResult = "{\"ErrorCode\":\"ESB-18-296\", \"ErrorMessage\":{\"Indonesian\":\"Terdapat indikasi tidak memiliki buku\" , \"English\":\"No book\"}}";
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"740,000,000.00\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"732,827,048.08\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"725,654,096.16\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"718,481,144.24\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"711,308,192.32\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"704,135,240.40\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"696,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"686,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"676,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"666,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"656,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"646,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"636,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"626,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"616,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"606,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"596,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"586,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"576,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"566,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"556,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"546,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"536,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"526,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"516,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"506,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"}]}";
		    }else if (mdlAPI.contentAPI.contains("0080002891")){
			jsonResult = "{\"ErrorCode\":\"ESB-18-293\", \"ErrorMessage\":{\"Indonesian\":\"Tidak ada transaksi yang dapat dicetak\" , \"English\":\"No transaction to print\"}}";
		    }
//		    else{
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"740,000,000.00\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"732,827,048.08\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"725,654,096.16\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"718,481,144.24\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"711,308,192.32\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"704,135,240.40\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"696,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"686,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"676,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"666,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"656,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"646,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"636,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"626,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"616,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"606,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"596,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"586,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"576,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"566,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"556,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"546,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"536,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"526,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"516,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"506,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"}]}";
//		    }
		    return jsonResult;
		}
//		
//		if (mdlAPI.urlAPI.contains("/alert/mm/status")){
//		    jsonResult = "{\"Status\":\"Taken\"}";
//		    return jsonResult;
//		}
//		if (mdlAPI.urlAPI.contentEquals("/login/login-by-card")){
//		    if (mdlAPI.contentAPI.contains("6019002501221560")){
//			jsonResult = "[{\"AccountNumber\":\"00080002891\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan\",\"AccountOwnership\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000003600\",\"CustomerName\":\"IUIOUIO\",\"RelationType\":\"5\",\"OwnerCode\":\"902\"},{\"CustomerNumber\":\"00000003551\",\"CustomerName\":\"LELIANI KUSUMA DEWI\",\"RelationType\":\"5\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"00100000181\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan Gold\",\"AccountOwnership\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000004741\",\"CustomerName\":\"IRMA HADINATA\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"00080004443\",\"AccountType\":\"107\",\"AccountTypeDescription\":\"TabunganKu\",\"AccountOwnership\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000006909\",\"CustomerName\":\"KATHERINE CLARACIA\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]}]";
//		    }else if (mdlAPI.contentAPI.contains("6019002501221545")){
//			jsonResult = "[{\"AccountNumber\":\"02580001508\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan\",\"AccountOwnership\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000004000\",\"CustomerName\":\"KARTONO-SOAP\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"02580001486\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan Gold\",\"AccountOwnership\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000004100\",\"CustomerName\":\"KARTONO-SOAP\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]}]";
//		    }else if (mdlAPI.contentAPI.contains("6019004500043276")){
//			jsonResult = "[{\"AccountNumber\":\"02580001176\",\"AccountType\":\"\",\"AccountTypeDescription\":\"\",\"AccountOwnership\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000005418\",\"CustomerName\":\"POPONG OTJE DJUNDJUNAN\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"02580001109\",\"AccountType\":\"\",\"AccountTypeDescription\":\"\",\"AccountOwnership\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000005418\",\"CustomerName\":\"POPONG OTJE DJUNDJUNAN\",\"RelationType\":\"3\",\"OwnerCode\":\"902\"},{\"CustomerNumber\":\"00000002005\",\"CustomerName\":\"NENGSIH FITRIA SITUNGKIR\",\"RelationType\":\"3\",\"OwnerCode\":\"901\"}]}]";
//		    }else if (mdlAPI.contentAPI.contains("6019004500008659")){
//			jsonResult = "[{\"AccountNumber\":\"07770001263\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan\",\"AccountOwnership\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000003277\",\"CustomerName\":\"ANI\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]}]";
//		    }
//		    return jsonResult;
//		}
//		else if(mdlAPI.urlAPI.contains("/deposit-accounts/account-number-lists/")){
//		    jsonResult = "{\"AccountList\":[{\"AccountNumber\":\"00210001395\",\"AccountName\":\"\",\"AccountType\":\"\",\"AccountTypeDescription\":\"Tahapan\",\"CustomerDetails\":[{\"CustomerNumber\":\"00000006508\",\"CustomerName\":\"JAYA KARINAA\",\"RelationType\":\"1\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"04010000021\",\"AccountName\":\"OMAR PUTIHRAI\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan\",\"CustomerDetails\":[{\"CustomerNumber\":\"00000000290\",\"CustomerName\":\"RICHARD MCAULIFFE\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]}]}";
//		    return jsonResult;		    
//		}
//		else if(mdlAPI.urlAPI.contentEquals("/passbooks/header")){
//		    jsonResult = "{\"AccountNumber\":\"00080002891\",\"AccountType\":\"110\",\"AccountName\":\"LELIANI KUSUMA DEWI\",\"ShortBranchName\":\"KCU BANDUNG\",\"BranchName\":\"KCU BANDUNG\",\"PageNumber\":\"01\",\"CustomerDetail\":[{\"CustomerNumber\":\"00000003551\",\"CustomerName\":\"LELIANI KUSUMA DEWI\",\"RelationType\":\"PEMILIK\",\"AccountOwnership\":\"P\",\"CustomerAddress\":[\"KECAMATAN TANAH ABANG\",\"JL KEBON KACANG 34\",\"\"]}]}";
//		    return jsonResult;
//		}
		
//		else if(mdlAPI.urlAPI.contains("/device-management/get-device-management")){
//		    jsonResult = "{\"SerialNumber\": \"D104182700226\",\"WSID\": \"00256777\",\"BranchCode\": \"0025\",\"BranchTypeID\": \"KM\",\"BranchInitial\": \"MKS\",\"BranchName\": \"Kas Mobil Makassar\",\"BranchTypeName\": \"Kas Mobil\",\"UpdateDate\": \"2018-06-22 10:42:16.075\",\"UpdateBy\": \"futurebranch\"}";
//		    return jsonResult;
//		}
//		else if (mdlAPI.urlAPI.contains("/rating/sequence") && mdlAPI.methodAPI.equalsIgnoreCase("POST")){
//		    jsonResult = "{\"Result\": \"SEQ-00215555-20190201-0001\"}";
//		    return jsonResult;
//		}
//		else if (mdlAPI.urlAPI.contains("/rating/sequence") && mdlAPI.methodAPI.equalsIgnoreCase("PUT")){
//		    jsonResult = "{\"Result\": \"Success\"}";
//		    return jsonResult;
//		}else if (mdlAPI.urlAPI.contains("/log/transaction-log")){
//			if (mdlAPI.contentAPI.contains("\"TransactionID\":\"\"")){
//			    jsonResult = "{\"Result\": \"TRA-00215555-20190201-00000001\"}";
//			}else{
//			    jsonResult = "{\"Result\": \"\"}";
//			}
//		    return jsonResult;
//		}
//		else if(mdlAPI.urlAPI.contentEquals("/alert/alert"))
//			jsonResult = "{\"Result\": \"ALT-002255WW-180912-00000007\"}";
//		if(mdlAPI.urlAPI.contentEquals("/device-management/login-device-management"))
//			jsonResult = "{\"Result\":\"true\"}";
//		else if(mdlAPI.urlAPI.contentEquals("/device-management/get-branch-list?serial=D104182700226&wsid=00220001"))
//			jsonResult = "[{\"BranchCode\":\"0001\",\"BranchName\":\"ASEMKA\",\"BranchTypeID\":\"KCU\",\"BranchTypeName\":\"Kantor Cabang Utama\",\"BranchInitial\":\"KPO\"}]";
//		else if(mdlAPI.urlAPI.contentEquals("/device-management/update-device-management"))
//			jsonResult = "{\"Result\":\"true\"}";
//		else if(mdlAPI.urlAPI.contentEquals("/passbooks/regular-passbooks/account-statements"))
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"3,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"734,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"4,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"738,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"5,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"743,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"6,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"749,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"7,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"756,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"8,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"764,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"9,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"773,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"10,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"783,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"}]}";
//		else if(mdlAPI.urlAPI.contentEquals("/passbooks/small-passbooks/account-statements"))
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"Transaction\":\"050417 KAS            4,000.00 K          610,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            5,000.00 K     615,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            6,000.00 K     621,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            7,000.00 K     628,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            8,000.00 K     636,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           10,000.00 K    646,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           11,000.00 K    657,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           12,000.00 K    669,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           13,000.00 K    682,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"}]}";
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"Transaction\":\"050417 KAS            4,000.00 K     610,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            5,000.00 K     615,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            6,000.00 K     621,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            7,000.00 K     628,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            8,000.00 K     636,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           10,000.00 K    646,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           11,000.00 K    657,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           12,000.00 K    669,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           13,000.00 K    682,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"}]}";
//		if(mdlAPI.urlAPI.equals("/alert/mm"))
//			jsonResult = "{\"ErrorCode\":\"MOMO-103-1020\",\"ErrorMessage\":{\"Indonesian\": \"Ada request lain dari perangkat yang sama belum lama ini\",\"English\": \"There was another request from this device recently\"}}";
//			jsonResult = "{\"ErrorCode\": \"MOMO-103-1012\",\"ErrorMessage\": {\"English\": \"AlertType / AccountType / TransactionType not match\",\"Indonesian\": \"AlertType / AccountType / TransactionType tidak sesuai\"}}";

		
		model.mdlToken mdlToken = TokenAdapter.GetToken();
		String urlAPI = mdlAPI.urlAPI;
		String urlFinal = APIGatewayIPAddress + urlAPI;
		String methodAPI = mdlAPI.methodAPI;
		String jsonIn = mdlAPI.contentAPI == null ? "" : mdlAPI.contentAPI;
		String version = mdlAPI.version == null ? "-" : mdlAPI.version;

		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		
		HostnameVerifier hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
		ClientConfig config = new DefaultClientConfig();
		SSLContext ctx = SSLContext.getInstance("SSL");
		TrustManager[] trustAllCerts = { new InsecureTrustManagerAdapter() };
		ctx.init(null, trustAllCerts, null);
		config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hostnameVerifier, ctx));
		
		Client client = Client.create(config);

//		Client client = Client.create();
		WebResource webResource = client.resource(urlFinal);
		ClientResponse apiResponse = null;

		//canonicalize JSON (remove all whitespace like \r, \n, \t and space)
		String bodyToHash = jsonIn.replaceAll("\\s+", "");
		model.mdlHeader mdlHeader = HeaderAdapter.GetHeaders(methodAPI, urlAPI, mdlToken.access_token, bodyToHash, decryptedAPIKey, decryptedAPISecret);
		
		try{
		    Builder builder = webResource.type("application/json")
				 .header("Authorization", "Bearer " + mdlToken.access_token)
				 .header("X-BCA-Key", mdlHeader.Key)
				 .header("X-BCA-Timestamp", mdlHeader.Timestamp)
				 .header("X-BCA-Signature", mdlHeader.Signature)
				 .header("X-BCA-ClientID", decryptedXBCAClientID);
		    
			 if (methodAPI.equalsIgnoreCase("POST")){
			     apiResponse = builder.post(ClientResponse.class,jsonIn);
			 }else if (methodAPI.equalsIgnoreCase("GET")){
			     apiResponse = builder.get(ClientResponse.class);
			 }else if (methodAPI.equalsIgnoreCase("PUT")){
			     apiResponse = builder.put(ClientResponse.class,jsonIn);
			 }else if (methodAPI.equalsIgnoreCase("DELETE")){
			     apiResponse = builder.delete(ClientResponse.class, jsonIn);
			 }

			 jsonResult = apiResponse.getEntity(String.class);
			 //check if authorized or not.. if not authorized, get new token and then call API again
			 String responseStatus = Integer.toString(apiResponse.getStatus());

			 if (responseStatus.equalsIgnoreCase("401") ){
				 try{
					 mdlErrorSchema = gson.fromJson(jsonResult, model.mdlErrorSchema.class);
				 }catch(Exception e){
				     	 stopTime = System.currentTimeMillis();
				     	 elapsedTime = stopTime - startTime;
					 logger.error("FAILED = ResponseTime : " + elapsedTime + ", version = " + version + ", URL :" + urlFinal + ", method: "+methodAPI+", jsonIn:" + jsonIn + ", X-BCA-Key:" + mdlHeader.Key
							 + ", X-BCA-Timestamp:"+ mdlHeader.Timestamp + ", X-BCA-Signature:" + mdlHeader.Signature
							 + ", X-BCA-ClientID:" + decryptedXBCAClientID + ", jsonOut:" + jsonResult + "Exception:" + e.toString(), e);
					 jsonResult = "{\"Error\":\"" + e.toString() + "\"}";
				}

				 if (mdlErrorSchema.ErrorCode != null && mdlErrorSchema.ErrorCode.equalsIgnoreCase("ESB-14-009") &&
			                        mdlErrorSchema.ErrorMessage.English.equalsIgnoreCase("Unauthorized")){
					mdlToken = TokenAdapter.GetNewToken();
					mdlHeader = HeaderAdapter.GetHeaders(methodAPI, urlAPI, mdlToken.access_token, bodyToHash, decryptedAPIKey, decryptedAPISecret);
					
					builder = webResource.type("application/json")
						.header("Authorization", "Bearer " + mdlToken.access_token)
						.header("X-BCA-Key", mdlHeader.Key)
						.header("X-BCA-Timestamp", mdlHeader.Timestamp)
						.header("X-BCA-Signature", mdlHeader.Signature)
						.header("X-BCA-ClientID", decryptedXBCAClientID);
					
					if (methodAPI.equalsIgnoreCase("POST")){
					    apiResponse = builder.post(ClientResponse.class, jsonIn);
					}else if (methodAPI.equalsIgnoreCase("GET")){
					    apiResponse = builder.get(ClientResponse.class);
					}else if (methodAPI.equalsIgnoreCase("PUT")){
					    apiResponse = builder.put(ClientResponse.class, jsonIn);
					}else if (methodAPI.equalsIgnoreCase("DELETE")){
					    apiResponse = builder.delete(ClientResponse.class, jsonIn);
					}
					jsonResult = apiResponse.getEntity(String.class);
					responseStatus = Integer.toString(apiResponse.getStatus());
				}
			 }
			 stopTime = System.currentTimeMillis();
		     	 elapsedTime = stopTime - startTime;
		     	 response.setStatus(apiResponse.getStatus());
			 logger.info("SUCCESS = ResponseTime : " + elapsedTime + ", ResponseStatus : " + responseStatus + ", version = " + version + ", callAPI = URL :"+urlFinal+", method: "+methodAPI + ", X-BCA-Key:" + mdlHeader.Key
					 + ", X-BCA-Timestamp:"+ mdlHeader.Timestamp + ", X-BCA-Signature:" + mdlHeader.Signature
					 + ", X-BCA-ClientID:" + decryptedXBCAClientID + ", jsonIn:" + jsonIn + ", jsonOut:" + jsonResult);
		}
		catch(Exception ex){
		    	stopTime = System.currentTimeMillis();
		     	elapsedTime = stopTime - startTime;
			logger.error("FAILED = ResponseTime : " + elapsedTime + ", version = " + version + ", URL :" + urlFinal + ", method: " + methodAPI + ", jsonIn:" + jsonIn + ", X-BCA-Key:" + mdlHeader.Key
					+ ", X-BCA-Timestamp:"+ mdlHeader.Timestamp + ", X-BCA-Signature:" + mdlHeader.Signature
					+ ", X-BCA-ClientID:" + decryptedXBCAClientID + ", jsonOut:" + jsonResult + "Exception:" + ex.toString(), ex);
			jsonResult = "{\"Error\":\"" + ex.toString() + "\"}";
			response.setStatus(apiResponse.getStatus());
		}

		return jsonResult;
	}
}
